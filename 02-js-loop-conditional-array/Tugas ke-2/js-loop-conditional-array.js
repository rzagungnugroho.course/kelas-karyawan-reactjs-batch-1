/** soal 1 */
let namaLengkap = "Riza Agung Nugroho"
console.log("soal 1")
console.log(namaLengkap)
console.log("================")

/** soal 2 */
let word = "JavaScript"
let second = "is"
let third = "awesome"

let outputGabunganVariable = word + " " + second + " " + third
console.log("soal 2")
console.log(outputGabunganVariable)
console.log("================")

/** soal 3 */
let hello = "Hello"
let world = "World!!!"

let output = `${hello} ${world}`
console.log("soal 3")
console.log(output)
console.log("==================")

/** soal 4 */
console.log("soal 4")

let panjangPersegiPanjang = "8"
let lebarPersegiPanjang = "5"
let kelilingPersegiPanjang = 2 * (parseInt(panjangPersegiPanjang) + parseInt(lebarPersegiPanjang))

console.log(kelilingPersegiPanjang)
console.log("=============")

/** soal 5 */
console.log("soal 5")

let sentences = 'wah javascript itu keren sekali';

let firstWords = sentences.substring(0, 3);
let secondWords = sentences.substring(4, 14); // do your own! 
let thirdWords = sentences.substring(15, 18);; // do your own! 
let fourthWords = sentences.substring(19, 23);; // do your own! 
let fifthWords = sentences.substring(24,);; // do your own! 

console.log('Kata Pertama: ' + firstWords);
console.log('Kata Kedua: ' + secondWords);
console.log('Kata Ketiga: ' + thirdWords);
console.log('Kata Keempat: ' + fourthWords);
console.log('Kata Kelima: ' + fifthWords);
console.log("=============")

// soal 6
console.log("Soal 6")

var sentence = "I am going to be React JS Developer";

var exampleFirstWord = sentence[0];
var exampleSecondWord = sentence[2] + sentence[3];
var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9]; // lakukan sendiri, wajib mengikuti seperti contoh diatas 
var fourthWord = sentence[11] + sentence[12]; // lakukan sendiri , wajib mengikuti seperti contoh diatas
var fifthWord = sentence[14] + sentence[15]; // lakukan sendiri , wajib mengikuti seperti contoh diatas
var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21] + sentence[22]; // lakukan sendiri , wajib mengikuti seperti contoh diatas
var seventhWord = sentence[23] + sentence[24]; // lakukan sendiri , wajib mengikuti seperti contoh diatas
var eighthWord = sentence[25] + sentence[26] + sentence[27] + sentence[28] + sentence[29] + sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34]; // lakukan sendiri , wajib mengikuti seperti contoh diatas

console.log('First Word: ' + exampleFirstWord);
console.log('Second Word: ' + exampleSecondWord);
console.log('Third Word: ' + thirdWord);
console.log('Fourth Word: ' + fourthWord);
console.log('Fifth Word: ' + fifthWord);
console.log('Sixth Word: ' + sixthWord);
console.log('Seventh Word: ' + seventhWord);
console.log('Eighth Word: ' + eighthWord)
console.log("============================")


//soal 7
console.log("Soal 7")


let txt = "I can eat bananas all day";
let hasil = txt.slice(10, 17); //lakukan pengambilan kalimat di variable ini

console.log(hasil)
console.log("============================")

//soal 8
console.log("Soal 8")


let nilaiDoe = 55;

if (nilaiDoe >= 80) {
    console.log("Nilai A")
} else if (nilaiDoe >= 70 && nilaiDoe < 80) {
    console.log("Nilai B")
} else if (nilaiDoe >= 60 && nilaiDoe < 70) {
    console.log("Nilai C")
} else if (nilaiDoe >= 50 && nilaiDoe < 60) {
    console.log("Nilai D")
} else {
    console.log("Nilai E")
}
console.log("============================")



//soal 9
console.log("Soal 9")


let angka = 2

let hasilAngka = angka === 2 ? "angka nya 2" : "bukan angka 2"
console.log(hasilAngka)
console.log("============================")

/** 
let angka = 2
if(angka === 2){
  console.log("angka nya 2")
}else{
  console.log("bukan angka 2")
}

var age= 20
var bisaVote= age > 20 ? "bisa vote" : "belum bisa vote" // hasilnya belum bisa vote
console.log(bisaVote)
var angka = 9
var jenisBilangan = angka % 2 === 0 ? "Bilangan Genap" : "Bukan Bilangan Genap" //hasilnya bukan bilangan genap
console.log(jenisBilangan)
*/

//soal 10 
console.log("Soal 10")


var traffic_lights = "red";

switch (traffic_lights) {
    case "red":
        console.log("Berhenti")
        break
    case "yellow":
        console.log("Hati-hati")
        break
    case "green":
        console.log("Berjalan")
        break
}
console.log("============================")


// soal 11
console.log("Soal 11")


for (let i = 1; i <= 10; i++) {
    console.log(i)
}
console.log("============================")


// soal 12
console.log("Soal 12")


for (let j = 1; j <= 10; j++) {
    if (j % 2 !== 0) {
        console.log(j + " Adalah bilangan ganjil")
    }
}
console.log("============================")


// soal 13
console.log("Soal 13")



for (let k = 1; k <= 10; k++) {
    if (k % 2 === 0) {
        console.log(k + " Adalah bilangan genap")
    }
}
console.log("=============================")

/**
// latiang penggabungan ganjil dan genap
for(let z = 1 ; z <= 10 ; z++){
     if(z % 2 !== 0){
          console.log(z + " bilangan ganjil")
     } //else(z % 2 === 0) --> genap
     else{
          console.log(z + " bilangan genap")
     }
}
*/

// soal 14
console.log("Soal 14")

let array = [1,2,3,4,5,6]

console.log(array[5])
console.log("=============================")

// soal 15
console.log("soal 15")

let array2 = [5,2,4,1,3,5]
array2.sort()

console.log(array2)
console.log("=============================")

// soal 16
console.log("soal 16")

let array3 = ["selamat", "anda", "melakukan", "perulangan", "array", "dengan", "for"]

for(let i = 0; i < array3.length; i++ ){
    console.log(array3[i])
}

console.log("=============================")

// soal 17
console.log("soal 17")

let array4 = [1, 2, 3, 4, 5, 6,7, 8, 9, 10]

for(let q = 0; q < array4.length; q++ ){
    if(array4[q] % 2 === 0 ){
        console.log(array4[q])
    }
}
console.log("=============================")

//soal 18
console.log("soal 18")

let kalimat= ["saya", "sangat", "senang", "belajar", "javascript"]

let slug = kalimat.join("-")
console.log(slug)
console.log("=============================")

//soal 19
console.log("soal 19")

var sayuran = []

sayuran.push = ['kangkung', 'bayam', 'buncis', 'kubis', 'timun', 'seledri', 'tauge']

console.log(sayuran)