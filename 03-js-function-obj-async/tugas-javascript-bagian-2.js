// soal 1
function cetakFunction() {
    console.log("Halo nama saya Riza Agung Nugroho")
}
cetakFunction()

// soal 2

function myFunction() {
    return angka1 + angka2
}
let angka1 = 20
let angka2 = 7
let output = (myFunction(angka1, angka2))

console.log(output)

// soal 3
let Hello = () => {
    return
}

Hello()


// const myFunction = () => {
//     let output = angka1 + angka2
// }
// let angka1 = 20
// let angka2 = 7
// let output = myFunction(angka1,angka2)
// console.log(output)

// soal 4
let arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku", 1992]
let objDaftarPeserta = {
    nama: arrayDaftarPeserta[0],
    "jenis kelamin": arrayDaftarPeserta[1],
    hobi: arrayDaftarPeserta[2],
    "tahun lahir": arrayDaftarPeserta[3]
}
console.log(objDaftarPeserta)


//soal 5
let buah = [
    { nama: "Nanas", warna: "Kuning", biji: false, harga: 9000 },
    { nama: "Jeruk", warna: "Oranye", biji: true, harga: 8000 },
    { nama: "Semangka", warna: "Hijau & Merah", biji: true, harga: "10000" },
    { nama: "Pisang", warna: "Kuning", biji: false, harga: "5000" }
]

let filterBijiBuah = buah.filter(function(item){
    return item.biji != true
})

console.log(filterBijiBuah)

// soal 6
let phone = {
    name: "Galaxy Note 20",
    brand: "Samsung",
    year: 2020
}

let { name, brand, year } = phone

console.log(name, brand, year)

// soal 7
let dataBukuTambahan = {
    penulis: "john doe",
    tahunTerbit: 2020
}

let buku = {
    nama: "pemograman dasar",
    jumlahHalaman: 172
}

let objOutput = { ...dataBukuTambahan, ...buku }
console.log(objOutput)

// soal 8
let mobil = {

    merk: "bmw",

    color: "red",

    year: 2002

}



const functionObject = (param) => {

    return param

}
console.log(functionObject(mobil)) 

