import React from "react";
import Artikel from "../components/Article";
import Hero from "../components/Hero";
import Navbar from "../components/Navbar";

const Tugas4 = () => {
    return (
        <>
            <Navbar />
            <Hero />
            <Artikel />
        </>
    )
}

export default Tugas4