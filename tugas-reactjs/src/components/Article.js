import React, { useState } from "react";
import Gambar from "../assets/smile.jpg"

const Artikel = () => {


    const [display, setDisplay] = useState(true)

    const handleDisplay = () => {
        setDisplay(display === true ? false : true)
    }

    return (
        <>
            <div className="article">
                <h1>Judul article</h1>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

                {display === true ?
                    <>
                        <img src={Gambar} />
                        <p><code>Posted By : Riza Agung Nugroho</code></p>
                    </>
                    :
                    ""
                }
                <button onClick={handleDisplay}>Menampilkan Pembuat Artikel</button>

            </div>
        </>
    )
}

export default Artikel