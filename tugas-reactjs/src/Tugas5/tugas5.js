import axios from "axios";
import React, { useEffect, useState } from "react";

const Tugas5 = () => {

     const [data, setData] = useState(null)

     //indikator
     const [fetchStatus, setFetchStatus] = useState(true)

     useEffect(() => {
          let fetchData = () => {
               axios.get('https://backendexample.sanbercloud.com/api/student-scores')
                    .then((res) => {
                         let resultData = res.data

                         setData([...resultData])
                    })
          }

          if (fetchStatus === true){
               fetchData()
               setFetchStatus(false)
          }

     }, [fetchStatus, setFetchStatus])

     console.log(data)

     const handleIndexScore = (param) => {
          if (param === null) {
               return ""
          } else if (param >= 80) {
               return "A"
          } else if (param >= 70 && param < 80) {
               return "B"
          } else if (param >= 60 && param < 70) {
               return "C"
          } else if (param >= 50 && param < 60) {
               return "D"
          } return "E"
     }

     return (
          <>
               <div className="container mx-auto">
                    <div className="overflow-x-auto relative shadow-md sm:rounded-lg">
                         <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                              <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                                   <tr>
                                        <th scope="col" className="py-3 px-6">
                                             No
                                        </th>
                                        <th scope="col" className="py-3 px-6">
                                             Nama
                                        </th>
                                        <th scope="col" className="py-3 px-6">
                                             Mata Kuliah
                                        </th>
                                        <th scope="col" className="py-3 px-6">
                                             Nilai
                                        </th>
                                        <th scope="col" className="py-3 px-6">
                                             Index Nilai
                                        </th>
                                   </tr>
                              </thead>
                              <tbody>

                                   {data !== null && data.map((res, index) => {
                                        return (
                                             <tr key={res.id} className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                                  <td className="py-4 px-6">
                                                       {index + 1}
                                                  </td>
                                                  <td className="py-4 px-6">
                                                       {res.name}
                                                  </td>
                                                  <td className="py-4 px-6">
                                                       {res.course}
                                                  </td>
                                                  <td className="py-4 px-6">
                                                       {res.score}
                                                  </td>
                                                  <td className="py-4 px-6">
                                                       {handleIndexScore(res.score)}
                                                  </td>
                                             </tr>
                                        )
                                   })}

                              </tbody>
                         </table>
                    </div>
               </div>
          </>
     )
}

export default Tugas5